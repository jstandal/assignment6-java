package com.company.assignment2bootlegitunes.data_access;

import com.company.assignment2bootlegitunes.models.*;

import java.util.ArrayList;

public interface SongRepository {
    public ArrayList<Song> getRandomSongsList();
    public ArrayList<Album> getRandomAlbumList();
    public ArrayList<Artist> getRandomArtistList();
    public ArrayList<Genre> getRandomGenreList();
    public ArrayList<SearchResults> getSearchResults(String search);
}
