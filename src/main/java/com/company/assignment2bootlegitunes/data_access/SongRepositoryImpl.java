package com.company.assignment2bootlegitunes.data_access;

import com.company.assignment2bootlegitunes.logging.LogToConsole;
import com.company.assignment2bootlegitunes.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;

//Handles all song information
@Repository
public class SongRepositoryImpl implements SongRepository{
    private final LogToConsole logger;

    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    public SongRepositoryImpl(LogToConsole logger) {
        this.logger = logger;
    }

    public ArrayList<Song> getRandomSongsList() {
        ArrayList<Song> songs = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Title FROM albums");
            // Execute Query

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                songs.add(
                        new Song(
                                resultSet.getString("Title")
                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }

        Collections.shuffle(songs);

        return new ArrayList<>(songs.subList(0, 5));
    }

    public ArrayList<Album> getRandomAlbumList() {
        ArrayList<Album> albums = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM Tracks");
            // Execute Query

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                albums.add(
                        new Album(
                                resultSet.getString("Name")
                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }

        Collections.shuffle(albums);

        return new ArrayList<>(albums.subList(0, 5));
    }

    public ArrayList<Artist> getRandomArtistList() {
        ArrayList<Artist> artists = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM artists");
            // Execute Query

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                artists.add(
                        new Artist(
                                resultSet.getString("Name")
                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }

        Collections.shuffle(artists);

        return new ArrayList<>(artists.subList(0, 5));
    }

    public ArrayList<Genre> getRandomGenreList() {
        ArrayList<Genre> genres = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Name FROM genres");
            // Execute Query

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genres.add(
                        new Genre(
                                resultSet.getString("Name")
                        ));
            }
            logger.log("Select all customers successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }

        Collections.shuffle(genres);

        return new ArrayList<>(genres.subList(0, 5));
    }

    public ArrayList<SearchResults> getSearchResults(String search){
        ArrayList<SearchResults> results = new ArrayList<>();
        try{
            // Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("""
                            SELECT tracks.Name as TrackTitle, a.Title as AlbumTitle, g.Name as GenreName, a2.Name as ArtistName
                            From tracks
                            JOIN albums a on tracks.AlbumId = a.AlbumId
                            JOIN artists a2 on a.AlbumId = a2.ArtistId
                            JOIN genres g on tracks.GenreId = g.GenreId
                            WHERE tracks.Name LIKE ?
                            GROUP BY tracks.Name""");
            // Execute Query
            preparedStatement.setString(1,"%"+search+"%");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                results.add(
                        new SearchResults(
                                resultSet.getString("TrackTitle"),
                                resultSet.getString("ArtistName"),
                                resultSet.getString("AlbumTitle"),
                                resultSet.getString("GenreName")
                        ));
            }
            logger.log("Get search result successful");
        }
        catch (Exception exception){
            logger.log(exception.toString());
        }
        finally {
            try {
                conn.close();
            }
            catch (Exception exception){
                logger.log(exception.toString());
            }
        }
        return results;
    }

}
