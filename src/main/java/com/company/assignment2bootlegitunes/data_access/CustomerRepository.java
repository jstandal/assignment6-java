package com.company.assignment2bootlegitunes.data_access;

import com.company.assignment2bootlegitunes.models.Country;
import com.company.assignment2bootlegitunes.models.Customer;
import com.company.assignment2bootlegitunes.models.CustomerGenre;
import com.company.assignment2bootlegitunes.models.CustomerInvoice;

import java.util.ArrayList;

public interface CustomerRepository {
    public ArrayList<Customer> getAllCustomers();
    public Customer getCustomerById(String customerId);
    public Boolean addCustomer(Customer customer);
    public Boolean updateCustomer(Customer customer);
    public ArrayList<Customer> getLimitedCustomers(String limit, String offset);
    public Customer getCustomerByName(String firstName);
    public ArrayList<Country> getCustomersInCountry();
    public ArrayList<CustomerInvoice> getTotalSpending();
    public CustomerGenre getFavouriteGenre(String customerId);
}
