package com.company.assignment2bootlegitunes.models;

public class Song {
    private String songTitle;

    public Song(String songTitle) {
        this.songTitle = songTitle;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public void setSongTitle(String songTitle) {
        this.songTitle = songTitle;
    }
}
