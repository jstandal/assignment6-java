package com.company.assignment2bootlegitunes.models;

public class Country {
    private String countryName;
    private String customerNumber;

    public Country(String countryName, String customerNumber) {
        this.countryName = countryName;
        this.customerNumber = customerNumber;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }
}
