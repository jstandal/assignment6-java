package com.company.assignment2bootlegitunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment2BootlegItunesApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assignment2BootlegItunesApplication.class, args);
	}

}
