package com.company.assignment2bootlegitunes.controllers.pages;

import com.company.assignment2bootlegitunes.data_access.SongRepository;
import com.company.assignment2bootlegitunes.data_access.SongRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class CustomerPageController {

    final
    SongRepositoryImpl songRepository;

    public CustomerPageController(SongRepositoryImpl songRepository) {
        this.songRepository = songRepository;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("songs", songRepository.getRandomSongsList());
        model.addAttribute("albums",songRepository.getRandomAlbumList());
        model.addAttribute("artists", songRepository.getRandomArtistList());
        model.addAttribute("genres", songRepository.getRandomGenreList());
        return "home";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(Model model, @RequestParam String searchText) {
        model.addAttribute("results", songRepository.getSearchResults(searchText));
        return "search";
    }

}
