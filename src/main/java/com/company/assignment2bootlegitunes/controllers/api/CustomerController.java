package com.company.assignment2bootlegitunes.controllers.api;

import com.company.assignment2bootlegitunes.data_access.CustomerRepository;
import com.company.assignment2bootlegitunes.models.Country;
import com.company.assignment2bootlegitunes.models.Customer;
import com.company.assignment2bootlegitunes.models.CustomerGenre;
import com.company.assignment2bootlegitunes.models.CustomerInvoice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {
    // Configure some endpoints to manage crud

    private final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(value = "/api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @RequestMapping(value = "/api/customers/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable String id){
        return customerRepository.getCustomerById(id);
    }

    @RequestMapping(value = "/api/customers/limit={limit}/offset={offset}")
    public ArrayList<Customer> getLimitedCustomers(@PathVariable String limit, @PathVariable String offset){
        return customerRepository.getLimitedCustomers(limit,offset);
    }

    @RequestMapping(value = "/api/customers/name={fname}")
    public Customer getCustomerByName(@PathVariable String fname){
        return customerRepository.getCustomerByName(fname);
    }

    @RequestMapping(value = "api/customers", method = RequestMethod.POST)
    public Boolean addNewCustomer(@RequestBody Customer customer){
        return customerRepository.addCustomer(customer);
    }

    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.PUT)
    public Boolean updateExistingCustomer(@PathVariable String id, @RequestBody Customer customer){
        return customerRepository.updateCustomer(customer);
    }

    @RequestMapping(value = "/api/countries", method = RequestMethod.GET)
    public ArrayList<Country> getCustomersInCountry() {
        return customerRepository.getCustomersInCountry();
    }

    @RequestMapping(value = "/api/customers/invoices", method = RequestMethod.GET)
    public ArrayList<CustomerInvoice> getTotalSpending() {
        return customerRepository.getTotalSpending();
    }

    @RequestMapping(value = "/api/customers/{id}/popular/genre", method = RequestMethod.GET)
    public CustomerGenre getFavouriteGenre(@PathVariable String id){
        return customerRepository.getFavouriteGenre(id);
    }

}

