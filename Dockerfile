FROM openjdk:16
ADD out/artifacts/assignment2_bootleg_itunes_jar/assignment2-bootleg-itunes.jar bootleg-itunes.jar
ENTRYPOINT ["java","-jar","bootleg-itunes.jar"]